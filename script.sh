#!/bin/bash

echo "Atualizando o Servidor"
echo "==============================================================================="

sudo apt-get update
sudo apt-get upgrade -y

echo "Instalando o Docker no Servidor"
echo "==============================================================================="

curl -fsSL https://get.docker.com -o get-docker.sh
sudo sh get-docker.sh

echo "Instalando o Gitlab-Runner no Servidor"
echo "==============================================================================="

curl -L "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh" | sudo bash

sudo apt-get install gitlab-runner -y

echo "==============================================================================="

echo "Script Finalizado no Servidor"
